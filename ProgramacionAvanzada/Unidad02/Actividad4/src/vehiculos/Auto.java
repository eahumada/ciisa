
package vehiculos;

public class Auto extends Vehiculo {

    public Auto(int codigo) {
        super(codigo, "Auto");
    }

    public Auto(int codigo, String marca, String modelo, int anio) {
        super(codigo, "Auto", marca, modelo, anio);
    }

    public Auto(int codigo, String marca, String modelo, int anio, double kilometraje, String patente) {
        super(codigo, "Auto", marca, modelo, anio, kilometraje, patente);
    }

}
