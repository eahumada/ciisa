package vehiculos;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Vehiculo extends Thread implements Comparable<Vehiculo>  {

    int codigo;
    String marca;
    String tipo;
    String modelo;
    int anio;
    double kilometraje;
    String patente;
    private List<Vehiculo> Stock;

    //Constructores
    public Vehiculo() {

    }

    public Vehiculo(int codigo, String tipo) {
        this.Stock = new ArrayList<Vehiculo>();
        this.codigo = codigo;
        this.tipo = tipo;
    }

    public Vehiculo(int codigo, String tipo, String marca, String modelo, int anio) {
        this(codigo, tipo);
        this.Stock = new ArrayList<Vehiculo>();
        this.marca = marca;
        this.modelo = modelo;
        this.anio = anio;
    }

    public Vehiculo(int codigo, String tipo, String marca, String modelo, int anio, double kilometraje, String patente) {
        this(codigo, tipo, marca, modelo, anio);
        this.Stock = new ArrayList<Vehiculo>();
        this.kilometraje = kilometraje;
        this.patente = patente;
    }

    //metodos setters
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public void setKilometraje(double kilometraje) {
        this.kilometraje = kilometraje;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

    public void setStock(List<Vehiculo> Stock) {
        this.Stock = Stock;
    }

    //metodos getters
    public int getCodigo() {
        return this.codigo;
    }

    public String getMarca() {
        return this.marca;
    }

    public String getTipo() {
        return this.tipo;
    }

    public String getModelo() {
        return this.modelo;
    }

    public int getAnio() {
        return this.anio;
    }

    public double getKilometraje() {
        return this.kilometraje;
    }

    public String getPatente() {
        return this.patente;
    }

    public List<Vehiculo> getStock() {
        return Stock;
    }

    @Override
    public String toString() {
        return "Codigo: " + codigo + "\nMarca: " + marca + "\nTipo: " + tipo + "\nModelo: " + modelo + "\nAño: " + anio + "\nKilometraje: " + kilometraje + "\nPatente=" + patente + "\n";
    }

    @Override
    public int compareTo(Vehiculo v) {

        int cmp;
        int a = anio;
        int b = v.anio;

        if (a > b) {
            cmp = +1;
        } else if (a < b) {
            cmp = -1;
        } else {
            
            int a2 = codigo;
            int b2 = v.codigo;

            if (a2 > b2) {
                cmp = +1;
            } else if (a2 < b2) {
                cmp = -1;
            } else {
                cmp = 0;
            }
            
        }

        return cmp;

    }

    @Override
    public void run() {
        try {
            int segundos = Math.round((float)Math.random()*10)+1;
            System.out.println("Partiendo a correr ["+this+"] por "+segundos+" segundos...");
            Thread.sleep(segundos*1000);
            System.out.println("Terminando de correr ["+this+"] por "+segundos+"segundos...");
        } catch (InterruptedException ex) {
            System.out.println("La carrera del vehiculo fue interrumpida.");
        }
    }

}
