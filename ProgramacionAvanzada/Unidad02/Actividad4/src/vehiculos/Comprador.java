
package vehiculos;

public class Comprador {

    String rut;
    String nombreComprador;
    String apellidoComprador;

    //Constructores
    public Comprador() {

    }

    public Comprador(String rut, String nombreComprador) {
        this.rut = rut;
        this.nombreComprador = nombreComprador;
    }

    public Comprador(String rut, String nombreComprador, String apellidoComprador) {
        this.rut = rut;
        this.nombreComprador = nombreComprador;
        this.apellidoComprador = apellidoComprador;
    }

    //metodos setters
    public void setRut(String rut) {
        this.rut = rut;
    }

    public void setNombreComprador(String nombreComprador) {
        this.nombreComprador = nombreComprador;
    }

    public void setApellidoComprador(String apellidoComprador) {
        this.apellidoComprador = apellidoComprador;
    }

    //metodos getters
    public String getRut() {
        return this.rut;
    }

    public String getNombreComprador() {
        return this.nombreComprador;
    }

    public String getApellidoComprador() {
        return this.apellidoComprador;
    }

    @Override
    public String toString() {
        return "Rut: " + rut + "\nNombre: " + nombreComprador + "\nApellido: " + apellidoComprador;
    }

}
