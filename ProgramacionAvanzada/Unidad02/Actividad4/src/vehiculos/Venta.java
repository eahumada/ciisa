
package vehiculos;

public class Venta {

    long monto;
    String fecha;
    Vehiculo vehiculo;
    Comprador comprador;

    //Constructores
    public Venta() {
    }
    public Venta(String fecha, Comprador comprador, Vehiculo vehiculo, long monto) {
        this.fecha = fecha;
        this.comprador = comprador;
        this.vehiculo = vehiculo;
        this.monto = monto;

    }

    //metodos setters
    public void setMonto(long monto) {
        this.monto = monto;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public void setComprador(Comprador comprador) {
        this.comprador = comprador;
    }

    //metodos getters
    public double getMonto() {
        return this.monto;
    }

    public String getFecha() {
        return this.fecha;
    }

    public Vehiculo getVehiculo() {
        return this.vehiculo;
    }

    public Comprador getComprador() {
        return this.comprador;
    }

    @Override
    public String toString() {
        return "Monto: " + monto + "\nFecha: " + fecha + "\nCod. Vehiculo: " + vehiculo.getCodigo()
                +"\nNombre: " + comprador.getNombreComprador() + "\nApellido: " + comprador.getApellidoComprador() + "\nRUT: " + comprador.getRut();
    }
}
