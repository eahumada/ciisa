
package vehiculos;

public class Moto extends Vehiculo {

    public Moto(int codigo) {
        super(codigo, "Moto");
    }

    public Moto(int codigo, String marca, String modelo, int anio) {
        super(codigo, "Moto", marca, modelo, anio);
    }

    public Moto(int codigo, String marca, String modelo, int anio, double kilometraje, String patente) {
        super(codigo, "Moto", marca, modelo, anio, kilometraje, patente);
    }

}
