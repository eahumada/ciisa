
package vehiculos;

public class Camioneta extends Vehiculo {

    public Camioneta(int codigo) {
        super(codigo, "Camioneta");
    }

    public Camioneta(int codigo, String marca, String modelo, int anio) {
        super(codigo, "Camioneta", marca, modelo, anio);
    }

    public Camioneta(int codigo, String marca, String modelo, int anio, double kilometraje, String patente) {
        super(codigo, "Camioneta", marca, modelo, anio, kilometraje, patente);
    }

}
