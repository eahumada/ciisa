/*
 * Integrantes:
 *
 * Daniel Espíndola Pizarro
 * Jepsy Daniela Lozada
 * Eduardo Ahumada
 *
 */
package actividad4;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import vehiculos.*;

public class Principal {

    public static Set<Vehiculo> filtrarPorTipo(Set<Vehiculo> stock, String tipo) {
        TreeSet<Vehiculo> filtrado = new TreeSet<Vehiculo>();

        for (Vehiculo v : stock) {
            if (v.getTipo().equalsIgnoreCase(tipo)) {
                filtrado.add(v);
            }
        }

        return filtrado;

    }

    public static Set<Vehiculo> filtrarVehiculosPorPatente(Set<Vehiculo> stock, List<Venta> ventas, String patente) {
        TreeSet<Vehiculo> filtrado = new TreeSet<Vehiculo>();
        
        for (Vehiculo v : stock) {
            if (v.getPatente().equalsIgnoreCase(patente)) {
                filtrado.add(v);
            }
        }
        
        for (Venta venta : ventas) {
            if (venta.getVehiculo()!=null) {
                if (venta.getVehiculo().getPatente().equalsIgnoreCase(patente)) {
                    filtrado.add(venta.getVehiculo());
                }
            }
        }
        
        return filtrado;

    }

    
    public static void main(String[] args) {

        //Lista de vehículos ordenada:
        TreeSet<Vehiculo> stock = new TreeSet<>();
        //Lista de Ventas:
        List<Venta> ventas = new ArrayList<>();
        
        try {
            
            System.out.println("=======================");
            System.out.println("INICIO DE LAS CASUISTICAS.");
            System.out.println("=======================\n");
            
            //Creación de Objetos comprador:
            Comprador comprador1 = new Comprador("12814696", "DANIELA", "LOZADA");
            Comprador comprador2 = new Comprador("13816631", "DANIEL", "ESPINDOLA");
            Comprador comprador3 = new Comprador("19813696", "EDUARDO", "AHUMADA");
            
            //Se escribe en pantalla detalle de Compradores.
            System.out.println("Comprador 1:\n" + comprador1 + "\n");
            System.out.println("Comprador 2:\n" + comprador2 + "\n");
            System.out.println("Comprador 3:\n" + comprador3 + "\n");
            
            //Creación de Objetos Vehículos e inclusión en la lista de Stock
            Vehiculo camioneta1 = new Camioneta(1, "Volkswagen", "Saveiro", 1989, 15000.0, "CXDS-0143");
            stock.add(camioneta1); //registra Vehículo en ArrayList "stock" con todos los datos en constructor
            
            Vehiculo moto2 = new Moto(2, "Harley Davison", "Tri Glide", 2019, 35000.0, "YHDS-0323");
            stock.add(moto2); //registra Vehículo en ArrayList "stock" con todos los datos en constructor
            
            Vehiculo auto3 = new Auto(3, "Ferrari", "Testa Rosa", 1982, 23000.0, "YHDX-0434");
            stock.add(auto3); //registra Vehículo en ArrayList "stock" con todos los datos en constructor
            
            Vehiculo moto4 = new Moto(4, "Yamaha", "Bolt", 2018, 0.0, "BOHS-1313");
            stock.add(moto4);//registra Vehículo en ArrayList "stock" con todos los datos en constructor
            
            Vehiculo auto5 = new Auto(5, "Audi", "R8", 2020, 0.0, "DBTG-0450");
            stock.add(auto5);//registra Vehículo en ArrayList "stock" con todos los datos en constructor
            
            Vehiculo auto6 = new Auto(6, "Ferrari", "Testa Rosa New", 1982, 23000.0, "YHDX-1434");
            stock.add(auto6);//registra Vehículo en ArrayList "stock" con todos los datos en constructor
            
            //Imprime por pantalla detalle de los Vehículos creados.
            System.out.println("Vehiculo 1:\n" + camioneta1);
            System.out.println("Vehiculo 2:\n" + moto2);
            System.out.println("Vehiculo 3:\n" + auto3);
            
            System.out.println("<< -- CASUÍSTICA #01 - CONSTRUCORES -->>\n");
            
            //Se crea objeto Venta con su respectivo constructor.
            Venta venta1 = new Venta("01-08-2019", comprador1, auto3, 6000000);
            //Imprime en pantalla datos del objeto: Venta1:
            System.out.println(venta1);
            
            System.out.println("\n<< -- CASUÍSTICA #02 - MÉTODOS SETTERS Y GETTERS -->>\n");
            
            //Se asignan valores a los atributos según método Setters
            venta1.setComprador(comprador1);
            venta1.setFecha("01/01/2019");
            venta1.setMonto(30000000);
            venta1.setVehiculo(camioneta1);
            
            //Nuevo Objeto venta
            Venta venta2 = new Venta();
            venta2.setComprador(comprador1);
            venta2.setFecha("01/01/2019");
            venta2.setMonto(35000000);
            venta2.setVehiculo(moto2);
            //Se imprime por pantalla datos de Venta 2:
            System.out.println(venta2);
            
            System.out.println("\n<< -- CASUÍSTICA #03 - TRY/CATCH -->>\n");
            
            Venta venta3 = new Venta();
            
            try {
            //Se asignan valores a los atributos con método Setter
            venta3.setComprador(comprador1);
            venta3.setFecha(null);
            venta3.setVehiculo(moto2);
            venta3.setMonto(30000000);
            
            //Se imprime por pantalla detalle de la venta3:
            System.out.println(venta3.getFecha().toString());
            
            } catch(NullPointerException e)
            {
                //Se maneja la excepción de error en fecha
                System.out.println("Fecha Null, Favor indicar nueva fecha.");
                venta3.setFecha("30/08/2019");
                venta3.setVehiculo(moto2);
                venta3.setMonto(30000000);                
            }
            
            //Se imprime por pantalla detalle de la venta3 con error en fecha corregido:
            System.out.println("\n" +venta3.toString());
            
            //Se agregan ventas a la lista de Vehículos Vendidos
            ventas.add(venta1);
            ventas.add(venta2);
            ventas.add(venta3);
            
            // Probamos haciendo realizando una carrera entre todos los vehiculos mientras se realizan las otras pruebas.
            System.out.println("Iniciando carrera:");
            for(Vehiculo vehiculo : stock) {
                vehiculo.start();
            }
            
        } catch (Exception e) {
            System.out.println("Ha ocurrido un error inesperado en la ejecucion de las casuisticas: " + e.toString() + " mensaje=" + e.getMessage());
        }
        
        System.out.println("\n<< -- CASUÍSTICA #04 - STOCK DE VEHÍCULOS POR AÑO -->>");
        
        System.out.println(stock.toString() + "\n");
        
        System.out.println("<< -- CASUÍSTICA #05 - STOCK DE VEHÍCULOS POR TIPO: MOTOS -->>\n");
        
        Set<Vehiculo> motos = filtrarPorTipo(stock, "Moto");        
        System.out.println(motos + "\n");
        
        System.out.println("<< -- CASUÍSTICA #06 - STOCK DE VEHÍCULOS POR TIPO: AUTOS -->>\n");
        
        Set<Vehiculo> autos = filtrarPorTipo(stock, "Auto");        
        System.out.println(autos + "\n");
        
        System.out.println("<< -- CASUÍSTICA #07- STOCK DE VEHÍCULOS POR TIPO: CAMIONETAS -->>\n");
        
        Set<Vehiculo> camionetas = filtrarPorTipo(stock, "camioneta");
        System.out.println(camionetas + "\n");
        
        System.out.println("<< -- CASUÍSTICA #08- BUSCAR VECÍCULO POR PATENTE -->> \n");
        Set<Vehiculo> porPatente = filtrarVehiculosPorPatente(stock, ventas, "YHDX-1434");
        System.out.println(porPatente + "\n");
        
        System.out.println("=======================");
        System.out.println("FIN DE LAS CASUISTICAS.");
        System.out.println("=======================");
        
        System.out.println("Esperando que termines los hilos");
        
        try {
            Thread.sleep(15000);
        } catch (InterruptedException ex) {
            System.out.println("La espera fue interrumpida.");
        }
        
        System.out.println("Espera terminada");
        

    }

}
